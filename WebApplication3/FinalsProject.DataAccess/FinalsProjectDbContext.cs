﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinalsProject.DB.Entities;

namespace FinalsProject.DataAccess
{
    class FinalsProjectDbContext : DbContext
    {
        public DbSet<Computer> Computers { get; set; }
        public DbSet<Processor> Processors { get; set; }
        public DbSet<ComputerBrand> ComputerBrands { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProcessorBrand> CPUBrands { get; set; }
    }
}
