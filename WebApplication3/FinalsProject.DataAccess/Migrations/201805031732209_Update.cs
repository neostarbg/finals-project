namespace FinalsProject.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "ComputerId", c => c.Int(nullable: false));
            CreateIndex("dbo.Products", "ComputerId");
            AddForeignKey("dbo.Products", "ComputerId", "dbo.Computers", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Products", "ComputerId", "dbo.Computers");
            DropIndex("dbo.Products", new[] { "ComputerId" });
            DropColumn("dbo.Products", "ComputerId");
        }
    }
}
