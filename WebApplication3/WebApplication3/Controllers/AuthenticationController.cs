﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication3.Controllers
{
    public class AuthenticationController : Controller
    {
		// GET: Authentication
		public ActionResult Index()
		{

            return View();
        }

		public ActionResult Success(string Message)
		{
			ViewBag.Message = Message;
			return View();
		}

		public ActionResult Login()
		{
			return View();
		}

		[HttpPost]
		public ActionResult Login(FormCollection form)
		{
			return View();
		}

		public ActionResult Register()
		{
			return View();
		}
		
		[HttpPost]
		public ActionResult Register(FormCollection form)
		{
			ViewBag.Message = form.AllKeys;

			return RedirectToAction("Success", new { Message = form["Email"]});
		}
    }
}