﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalsProject.DB.Entities
{
    public class Processor : BaseEntity
    {
        [Required, MaxLength(20)]
        public string Model { get; set; }

        [Required]
        public int ProcessorBrandId { get; set; }

        [Required, ForeignKey("ProcessorBrandId")]
        public virtual ProcessorBrand brand { get; set; }

        [Required]
        public int Cores { get; set; }
        [Required]
        public int Threads { get; set; }
        [Required]
        public decimal Frequency { get; set; }
    }
}
