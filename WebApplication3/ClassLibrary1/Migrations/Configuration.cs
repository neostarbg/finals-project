using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using FinalsProject.DataAccess.Entities;

namespace FinalsProject.DataAccess.Migrations
{
    

    internal sealed class Configuration : DbMigrationsConfiguration<FinalsProject.DataAccess.CustomDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
			AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(FinalsProject.DataAccess.CustomDbContext context)
        {
            if(!context.Processors.Any())
            {
                context.Processors.AddOrUpdate(x => x.Id,
                    new Processor() { Id = 1, Name = "Intel Core i5 5800 ", Cores = 4, Frequency = 3500, Threads = 8 },
                    new Processor() { Id = 2, Name = "AMD FX-6300 ", Cores = 6, Frequency = 3500, Threads = 6 },
                    new Processor() { Id = 3, Name = "Intel Alabala", Cores = 12, Frequency = 3000, Threads = 24 },
                    new Processor() { Id = 4, Name = "asd", Cores = 4, Frequency = 3500, Threads = 8 },
                    new Processor() { Id = 5, Name = "fgh", Cores = 4, Frequency = 3500, Threads = 8 }
                );

				context.SaveChanges();
            }

			if(!context.Computers.Any())
			{
				context.Computers.AddOrUpdate(x => x.Id,
					new Computer() { Id = 1, HardDriveCapacity = 500, RamCapacity = 4, ProcessorId = 1}, 
					new Computer() { Id = 2, HardDriveCapacity = 1000, RamCapacity = 8, ProcessorId = 2},
					new Computer() { Id = 3, HardDriveCapacity = 1500, RamCapacity = 32, ProcessorId = 3}
				);

				context.SaveChanges();
			}

			if(!context.Products.Any())
			{
				context.Products.AddOrUpdate(x => x.Id, 
					new Product() { Id = 1, ComputerId = 1, Description = "Dobur komp za ofis", IsAvailable = 1, Name = "Dell Inspiron D231", Price = 349.99M },
					new Product() { Id = 2, ComputerId = 2, Description = "Gaming Computer ", IsAvailable = 1, Name = "Acer Alienware Desktop x1", Price = 799.99M},
					new Product() { Id = 3, ComputerId = 3, Description = "Elite Server Hardware for performace and stuff", IsAvailable = 0, Name = "HP Workstation 3", Price = 1599.99M}
				);

				context.SaveChanges();
			}
        }
    }
}
