﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
namespace FinalsProject.DataAccess.Entities
{
    public class Product : BaseEntity
    {
        [Required, StringLength(30)]
        public string Name { get; set; }

        [Required, StringLength(500)]
        public string Description { get; set; }

        public decimal Price { get; set; }

        public int IsAvailable { get; set; }

        [Required]
        public int ComputerId { get; set; }
        [ForeignKey("ComputerId")]
        public virtual Computer Computer { get; set; }

		public int Rating { get; set; }

    }
}
