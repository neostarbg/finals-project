﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FinalsProject.DataAccess.Entities
{
    public class Processor : BaseEntity
    {
        [Required, StringLength(20)]
        public string Name { get; set; }
        public int Frequency { get; set; }
        public int Cores { get; set; }
        public int Threads { get; set; }
    }
}
