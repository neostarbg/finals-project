﻿
using System.Data.Entity;
using FinalsProject.DataAccess.Entities;

namespace FinalsProject.DataAccess
{
    class DbContextA : DbContext
    {
        public DbSet<Processor> Processors { get; set; }
        public DbSet<Computer> Computers { get; set; }
        public DbSet<Product> Products { get; set; }
    }
}
