﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using FinalsProject.DataAccess.Entities;

namespace FinalsProject.DataAccess
{
    public class CustomDbContext : DbContext
    {
        public DbSet<Processor> Processors { get; set; }
        public DbSet<Computer> Computers { get; set; }
        public DbSet<Product> Products { get; set; }

		public System.Data.Entity.DbSet<FinalsProject.DataAccess.Entities.User> Users { get; set; }

		public System.Data.Entity.DbSet<FinalsProject.DataAccess.Entities.Role> Roles { get; set; }
	}
}
