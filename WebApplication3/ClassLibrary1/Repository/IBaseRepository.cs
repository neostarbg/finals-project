﻿using FinalsProject.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace FinalsProject.DataAccess.Repository
{
    interface IBaseRepository<T> where T : class
    {

        T GetByID(int id);

        void Save(T item);

        void Create(T item);
		void Update(T item, Func<T, bool> findByIDPredecate);
        //void Update(T item, Func<T, bool>);
    }
}
