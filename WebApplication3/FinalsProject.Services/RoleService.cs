﻿using FinalsProject.DataAccess.Entities;
using FinalsProject.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalsProject.Services
{
	public class RoleService : BaseService<Role>
	{
		private RoleRepository repository;

		public RoleService()
		{
			repository = new RoleRepository();
		}

		public new List<Role> GetAll()
		{
			return repository.GetAll();
		}

		public new Role GetByID(int id)
		{
			return repository.GetByID(id);
		}

		public new void Save(Role role)
		{
			repository.Save(role);
		}
	}
}
